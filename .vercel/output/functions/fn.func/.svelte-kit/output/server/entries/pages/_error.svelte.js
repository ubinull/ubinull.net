import { g as getContext, c as create_ssr_component, f as subscribe, e as escape } from "../../chunks/ssr.js";
import { p as pageData } from "../../chunks/index3.js";
const getStores = () => {
  const stores = getContext("__svelte__");
  return {
    /** @type {typeof page} */
    page: {
      subscribe: stores.page.subscribe
    },
    /** @type {typeof navigating} */
    navigating: {
      subscribe: stores.navigating.subscribe
    },
    /** @type {typeof updated} */
    updated: stores.updated
  };
};
const page = {
  subscribe(fn) {
    const store = getStores().page;
    return store.subscribe(fn);
  }
};
const Error$1 = create_ssr_component(($$result, $$props, $$bindings, slots) => {
  let $page, $$unsubscribe_page;
  $$unsubscribe_page = subscribe(page, (value) => $page = value);
  pageData.set({
    id: "error",
    title: "Error",
    label: "Oh no! Something bad happened."
  });
  $$unsubscribe_page();
  return `<div class="w-[100%] h-[100%] flex items-center justify-center"><span class="text-center"><h1 class="text-3xl font-extrabold text-red-700">${escape($page.status)}: ${escape($page.error?.message)}</h1> <a href="/" class="hover:underline" data-svelte-h="svelte-1gnluhg">Go back home</a></span></div>`;
});
export {
  Error$1 as default
};
