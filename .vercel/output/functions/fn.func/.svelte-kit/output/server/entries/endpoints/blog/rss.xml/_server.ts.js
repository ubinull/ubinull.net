async function GET({ fetch }) {
  const response = await fetch("api/posts");
  const posts = await response.json();
  const headers = { "Content-Type": "application/xml" };
  const xml = `
		<rss xmlns:atom="http://www.w3.org/2005/Atom" version="2.0">
			<channel>
				<title>ubinull's blog</title>
				<description>ubinull's personal blog for his opinions no one asked for</description>
				<link>https://ubinull.vercel.app/</link>
				<atom:link href="https://ubinull.vercel.app/blog/rss.xml" rel="self" type="application/rss+xml"/>
				${posts.map(
    (post) => `
						<item>
							<title>${post.title}</title>
							<description>${post.description}</description>
							<link>https://ubinull.vercel.app/blog/${post.slug}</link>
							<guid isPermaLink="true">https://ubinull.vercel.app/blog/${post.slug}</guid>
							<pubDate>${new Date(post.date).toUTCString()}</pubDate>
						</item>
					`
  ).join("")}
			</channel>
		</rss>
	`.trim();
  return new Response(xml, { headers });
}
export {
  GET
};
