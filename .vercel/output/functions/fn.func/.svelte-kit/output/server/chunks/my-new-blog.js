import { c as create_ssr_component } from "./ssr.js";
const metadata = {
  "title": "My new blog",
  "description": "I made a new blog with SvelteKit",
  "tags": ["programming", "sveltekit", "webdev"],
  "date": 1689610370,
  "published": true,
  "image": "/placeholder.png"
};
const My_new_blog = create_ssr_component(($$result, $$props, $$bindings, slots) => {
  return `<h1 data-svelte-h="svelte-1j03hfj">h1</h1> <h2 data-svelte-h="svelte-x65818">h2</h2> <h3 data-svelte-h="svelte-9090jx">h3</h3> <h4 data-svelte-h="svelte-6jng9e">h4</h4> <h5 data-svelte-h="svelte-1b9bf97">h5</h5> <h6 data-svelte-h="svelte-pfd69k">h6</h6> <p data-svelte-h="svelte-1ek0fsr">It wasn’t easy.</p>`;
});
const __vite_glob_0_1 = /* @__PURE__ */ Object.freeze(/* @__PURE__ */ Object.defineProperty({
  __proto__: null,
  default: My_new_blog,
  metadata
}, Symbol.toStringTag, { value: "Module" }));
export {
  __vite_glob_0_1 as _
};
