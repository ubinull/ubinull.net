import { c as create_ssr_component } from "./ssr.js";
const metadata = {
  "title": "My experience with the Raspberry PI",
  "description": "What it's good for, what it's not good for and other fun tales.",
  "tags": ["programming", "hardware", "Raspberry PI"],
  "date": 1689610370,
  "published": true,
  "image": "/placeholder.png"
};
const My_experience_with_the_raspberry_pi = create_ssr_component(($$result, $$props, $$bindings, slots) => {
  return `<h1 data-svelte-h="svelte-o3snbg">My experience with the raspberry pi</h1> <!-- HTML_TAG_START -->${`<pre class="shiki github-dark" style="background-color: #24292e" tabindex="0"><code><span class="line"><span style="color: #F97583">const</span><span style="color: #E1E4E8"> </span><span style="color: #79B8FF">discordjs</span><span style="color: #E1E4E8"> </span><span style="color: #F97583">=</span><span style="color: #E1E4E8"> </span><span style="color: #B392F0">require</span><span style="color: #E1E4E8">(</span><span style="color: #9ECBFF">&#39;discord.js&#39;</span><span style="color: #E1E4E8">);</span></span>
<span class="line"></span>
<span class="line"><span style="color: #F97583">const</span><span style="color: #E1E4E8"> </span><span style="color: #79B8FF">client</span><span style="color: #E1E4E8"> </span><span style="color: #F97583">=</span><span style="color: #E1E4E8"> </span><span style="color: #F97583">new</span><span style="color: #E1E4E8"> </span><span style="color: #B392F0">DiscordClient</span><span style="color: #E1E4E8">(() </span><span style="color: #F97583">=&gt;</span><span style="color: #E1E4E8"> &#123;</span></span>
<span class="line"><span style="color: #E1E4E8">    on.</span><span style="color: #B392F0">ready</span><span style="color: #E1E4E8">(() </span><span style="color: #F97583">=&gt;</span><span style="color: #E1E4E8"> &#123;</span></span>
<span class="line"><span style="color: #E1E4E8">        console.</span><span style="color: #B392F0">log</span><span style="color: #E1E4E8">(</span><span style="color: #9ECBFF">&quot;Ready!!&quot;</span><span style="color: #E1E4E8">);</span></span>
<span class="line"><span style="color: #E1E4E8">    &#125;)</span></span>
<span class="line"><span style="color: #E1E4E8">&#125;)</span></span></code></pre>`}<!-- HTML_TAG_END --> <p data-svelte-h="svelte-zz34m">I liked it.
Also, did you know that when people say they like something <strong>they actually like something</strong>.</p>`;
});
const __vite_glob_0_0 = /* @__PURE__ */ Object.freeze(/* @__PURE__ */ Object.defineProperty({
  __proto__: null,
  default: My_experience_with_the_raspberry_pi,
  metadata
}, Symbol.toStringTag, { value: "Module" }));
export {
  __vite_glob_0_0 as _
};
