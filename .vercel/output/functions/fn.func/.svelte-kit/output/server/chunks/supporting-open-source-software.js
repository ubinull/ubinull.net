import { c as create_ssr_component } from "./ssr.js";
const metadata = {
  "title": "Supporting FLOSS",
  "description": "FLOSS = Free, Libre, and Open Source software",
  "tags": ["floss", "support", "programming"],
  "date": 1689610371,
  "published": true,
  "image": "/placeholder.png"
};
const Supporting_open_source_software = create_ssr_component(($$result, $$props, $$bindings, slots) => {
  return `<h1 data-svelte-h="svelte-12qg95j">I like FLOSS.</h1> <p data-svelte-h="svelte-z34ocz">Please support it!</p>`;
});
const __vite_glob_0_2 = /* @__PURE__ */ Object.freeze(/* @__PURE__ */ Object.defineProperty({
  __proto__: null,
  default: Supporting_open_source_software,
  metadata
}, Symbol.toStringTag, { value: "Module" }));
export {
  __vite_glob_0_2 as _
};
