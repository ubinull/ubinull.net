import * as universal from '../entries/pages/_layout.ts.js';

export const index = 0;
let component_cache;
export const component = async () => component_cache ??= (await import('../entries/pages/_layout.svelte.js')).default;
export { universal };
export const universal_id = "src/routes/+layout.ts";
export const imports = ["_app/immutable/nodes/0.c354ab78.js","_app/immutable/chunks/scheduler.2100a7f6.js","_app/immutable/chunks/index.a3f99c0f.js","_app/immutable/chunks/index.4daa6eb4.js","_app/immutable/chunks/index.3109faae.js"];
export const stylesheets = ["_app/immutable/assets/0.39e7f3ad.css"];
export const fonts = [];
