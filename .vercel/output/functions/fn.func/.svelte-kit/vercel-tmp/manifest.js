export const manifest = (() => {
function __memo(fn) {
	let value;
	return () => value ??= (value = fn());
}

return {
	appDir: "_app",
	appPath: "_app",
	assets: new Set(["background.png","blog.svg","close.svg","favicon.png","github.svg","gitlab.svg","home.svg","info.svg","kofi.svg","mastodon.svg","pills-d.avif","placeholder.png","projects.svg","robots.txt","rotate_screen.svg","twitter.svg"]),
	mimeTypes: {".png":"image/png",".svg":"image/svg+xml",".avif":"image/avif",".txt":"text/plain"},
	_: {
		client: {"start":"_app/immutable/entry/start.87ec0afc.js","app":"_app/immutable/entry/app.e282cb26.js","imports":["_app/immutable/entry/start.87ec0afc.js","_app/immutable/chunks/scheduler.2100a7f6.js","_app/immutable/chunks/singletons.c3e9ee8b.js","_app/immutable/chunks/index.3109faae.js","_app/immutable/chunks/control.f5b05b5f.js","_app/immutable/entry/app.e282cb26.js","_app/immutable/chunks/preload-helper.cf010ec4.js","_app/immutable/chunks/scheduler.2100a7f6.js","_app/immutable/chunks/index.a3f99c0f.js"],"stylesheets":[],"fonts":[]},
		nodes: [
			__memo(() => import('../output/server/nodes/0.js')),
			__memo(() => import('../output/server/nodes/1.js'))
		],
		routes: [
			{
				id: "/api/posts",
				pattern: /^\/api\/posts\/?$/,
				params: [],
				page: null,
				endpoint: __memo(() => import('../output/server/entries/endpoints/api/posts/_server.ts.js'))
			},
			{
				id: "/blog/rss.xml",
				pattern: /^\/blog\/rss\.xml\/?$/,
				params: [],
				page: null,
				endpoint: __memo(() => import('../output/server/entries/endpoints/blog/rss.xml/_server.ts.js'))
			}
		],
		matchers: async () => {
			
			return {  };
		}
	}
}
})();
