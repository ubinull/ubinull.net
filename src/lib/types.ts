export type Post = {
    title: string,
    slug: string,
    description: string,
    date: string,
    categories: object,
    published: boolean,
    image: string,
}