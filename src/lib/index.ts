import { writable } from 'svelte/store';

export const pageData = writable({
	id: '',
	title: '',
	label: ''
});
